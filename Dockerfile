FROM node:12
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn --frozen-lockfile
COPY . ./
RUN yarn build
EXPOSE 3000
CMD yarn start
# FROM node:12
# RUN mkdir /app
# WORKDIR /app
# COPY package.json /app
# RUN yarn install
# COPY . /app
# RUN yarn test
# RUN yarn build
# CMD yarn start
# EXPOSE 3000
